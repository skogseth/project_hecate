// Changes to pendorite ingot recipes

craftingTable.removeRecipe(<item:byg:pendorite_ingot>);

craftingTable.addShaped("pendorite_ingot_from_scraps", <item:byg:pendorite_ingot>, [
[<item:byg:pendorite_scraps>, <item:byg:pendorite_scraps>, <item:byg:pendorite_scraps>],
[<item:byg:pendorite_scraps>, <item:endreborn:tungsten_ingot>, <item:endreborn:tungsten_ingot>],
[<item:endreborn:tungsten_ingot>, <item:endreborn:tungsten_ingot>, <item:minecraft:air>]
]);

craftingTable.addShapeless("pendorite_ingot_from_block", <item:byg:pendorite_ingot>*9, [<item:byg:pendorite_block>]);



// Pendorite scrap generation

// ??
