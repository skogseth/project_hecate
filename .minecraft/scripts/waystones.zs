// Replace recipe for warp dust

craftingTable.removeRecipe(<item:waystones:warp_dust>);

craftingTable.addShapeless("warp_dust", <item:waystones:warp_dust>, [<item:minecraft:ender_pearl>, <tag:items:endreborn:inventory_hammers>]);



// Remove recipes for scrolls

craftingTable.removeRecipe(<item:waystones:return_scroll>);
craftingTable.removeRecipe(<item:waystones:bound_scroll>);
craftingTable.removeRecipe(<item:waystones:warp_scroll>);



// Replace recipe for warp stone

craftingTable.removeRecipe(<item:waystones:warp_stone>);

craftingTable.addShaped("warp_stone", <item:waystones:warp_stone>, [
[<item:waystones:warp_dust>, <item:minecraft:diamond>, <item:waystones:warp_dust>],
[<item:minecraft:diamond>, <item:minecraft:nether_star>, <item:minecraft:diamond>],
[<item:waystones:warp_dust>, <item:minecraft:diamond>, <item:waystones:warp_dust>]
]);



// Replace recipe for warp plate

craftingTable.removeRecipe(<item:waystones:warp_plate>);

craftingTable.addShaped("warp_plate", <item:waystones:warp_plate>, [
[<item:minecraft:stone_bricks>, <item:minecraft:stone_bricks>, <item:minecraft:stone_bricks>],
[<item:minecraft:stone_bricks>, <item:waystones:warp_stone>, <item:minecraft:stone_bricks>],
[<item:minecraft:stone_bricks>, <item:minecraft:stone_bricks>, <item:minecraft:stone_bricks>]
]);
