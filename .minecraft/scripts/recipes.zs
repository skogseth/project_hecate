furnace.removeRecipe(<item:minecraft:netherite_scrap>);

craftingTable.removeRecipe(<item:byg:pendorite_horse_armor>);
craftingTable.removeRecipe(<item:byg:ametrine_horse_armor>);

//smithing.addRecipe("ametrine_horse_armor", <item:byg:ametrine_horse_armor>, <item:netheritehorsearmor:netherite_horse_armor>, <items:byg:ametrine_gems>);

//craftingTable.removeRecipe(<item:eidolon:pewter_blend>);
//craftingTable.addShapeless("pewter_blend", <item:eidolon:pewter_blend>, [<item:create:crushed_lead_ore>, <item:create:crushed_iron_ore>]);

craftingTable.addShaped("copper_chimes", <item:chimes:copper_chimes>, [
[<item:minecraft:air>, <tag:items:minecraft:wooden_slabs>, <item:minecraft:air>],
[<item:minecraft:air>, <item:minecraft:chain>, <item:minecraft:air>],
[<item:minecraft:air>, <item:create:copper_ingot>, <item:minecraft:air>]
]);
